<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Post;
use DB;

class PostsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() 
    {
        $this->middleware('auth',['except' => ['index','show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // pega todos os posts
        // $posts = Post::all()
        // pega os posts ordernando pelo titulo (asc ou desc)
        // $posts = Post::orderBy('title', 'desc')->get();
        // clausula where - filtrando somente um post
        // return $post = Post::where('title', 'Post Two')->get();

        // como usar comando SQL puro - importar no inicio o use DB;
        // $posts = DB::select('SELECT * FROM posts');

        // limitando quantidade de posts
        // $posts = Post::orderBy('title', 'desc')->take(1)->get();

        // paginate - cria o indice de pagina
        $posts = Post::orderBy('created_at', 'desc')->paginate(10);
        return view('posts.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:1999'
        ]);

        // handle file upload
        // verifica se fez upload de algum arquivo
        if($request->hasFile('cover_image')){
            // pega o nome do arquivo e extensao
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            // pegar somente o nome
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // pegar somente a extensao 
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            // arquivo para armazenar - time() timestamp - mesmo se fizer upload com mesm o nome, nao vai ter problema
            $fileNameToStore = $filename .'_'. time() .'.'. $extension;
            // upload a image
            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);
        } else {
            // se nao fizer, vai esta imagem padrao
            $fileNameToStore = 'noimage.jpg';

        }

        // create post
        $post = new Post();
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id;
        $post->cover_image = $fileNameToStore;
        $post->save();

        return redirect('/posts')->with('success', 'Post Criado');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

 

        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        // handle file upload
        // verifica se fez upload de algum arquivo
        if($request->hasFile('cover_image')){
            // pega o nome do arquivo e extensao
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            // pegar somente o nome
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // pegar somente a extensao 
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            // arquivo para armazenar - time() timestamp - mesmo se fizer upload com mesm o nome, nao vai ter problema
            $fileNameToStore = $filename .'_'. time() .'.'. $extension;
            // upload a image
            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);
        } 

        // Update post
        $post = Post::find($id);
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        if($request->hasFile('cover_image')){
            $post->cover_image = $fileNameToStore;
        }
        $post->save();

        return redirect('/posts')->with('success', 'Post Atualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        
        // check se é o usuario correto
        if(auth()->user()->id  !== $post->user_id) {
            return redirect('/posts')->with('error', 'Não Autorizado');
        }

        if($post->cover_image != 'noimage.jpg') {
            // delete a imagem
            Storage::delete('public/cover_images/'. $post->cover_image);

        }
        
        $post->delete();

        return redirect('/posts')->with('success', 'Post Deletado');
    }
}
