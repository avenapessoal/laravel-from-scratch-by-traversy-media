<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{

    public function index()
    {
        $title = 'Bem-vindo ao Laravel!';
        return view('pages.index')->with('title', $title); // works ok
                                                               // return view('pages.index', compact('title')); // works ok
                                                               // return 'INDEX Controller'; // works ok
    }

    public function about()
    {
        $title = 'About Us!';
        return view('pages.about')->with('title', $title); // work ok
                                                               // return view('pages.about', compact('title')); // work ok
                                                               // return view('pages.about'); // works ok
    }

    public function services()
    {
        $data = array(
            'title' => 'Services!!',
            'services' => [
                'Web',
                'Programing',
                'SEO'
            ]
        );
        return view('pages.services')->with($data); // work
    }
}
