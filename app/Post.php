<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    // Table name - meio que criado automatico...somente mostrando que pode definir
    protected $table = 'posts';

    // primary key - meio que criado automatico...somente mostrando que pode definir
    public $primaryKey = 'id';

    // timestramps - meio que criado automatico...somente mostrando que pode definir
    public $timestramps = true;

    public function user() {
        return $this->belongsTo('App\User');
    }
} 