<?php

/*
 * |--------------------------------------------------------------------------
 * | Web Routes
 * |--------------------------------------------------------------------------
 * |
 * | Here is where you can register web routes for your application. These
 * | routes are loaded by the RouteServiceProvider within a group which
 * | contains the "web" middleware group. Now create something great!
 * |
 */

/*
 * Route::get('/', function () {
 * return view('welcome');
 * });
 *
 */

// Place all your web routes here...(Cut all `Route` which are define in `Route file`, paste here)
Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/services', 'PagesController@services');

Route::resource('posts', 'PostsController');


/*
Route::get('/about', function () {
    return view('pages.about');
});

Route::get('/services', function () {
    return view('pages.services');
});
*/



    /*
     * Route::get('/users/{id}/{name}', function ($id, $name) {
     * return 'Seu nome eh ' . ucfirst($name) . ' e seu id eh ' . $id;
     * });
     */

Auth::routes();

Route::get('/dashboard', 'DashboardController@index');
 